
const Ingredients = [
  'Булка',
  'Огурчик',
  'Котлетка',
  'Бекон',
  'Рыбная котлета',
  'Соус карри',
  'Кисло-сладкий соус',
  'Помидорка',
  'Маслины',
  'Острый перец',
  'Капуста',
  'Кунжут',
  'Сыр Чеддер',
  'Сыр Виолла',
  'Сыр Гауда',
  'Майонез',
  'Кетчуп'
];

let OurMenu = [];
let OurOrders = [];

class Burger {

  constructor(name, ingredients, cookingTime, menu = true) {
    this.name = name;
    this.ingredients = ingredients;
    this.cookingTime = cookingTime;

    const newBurger = {
      name: this.name,
      ingredients: this.ingredients,
      cookingTime: this.cookingTime,
      showIngredients: this.showIngredients
    };

    if (menu === true) OurMenu.push(newBurger);
    this.addToOrder = this.addToOrder.bind(this);
    this.renderMenu(menuCover);
  }

  renderMenu(target) {
    let menuItem = document.createElement('div');

    menuItem.innerHTML = `
        <h3>${this.name}</h3>
        <p>Состав: ${this.ingredients.join(', ')}</p>
        <button class="addToOrder">Заказать</button>`;

    menuItem.querySelector('.addToOrder').addEventListener('click', this.addToOrder);
    target.appendChild(menuItem);
  }

  addToOrder() {
    let target = this.name;

    let currentBurger = OurMenu.filter(function (item) {
      if (target === item.name) {
        return item.name;
      }
    });

    new Order(currentBurger[0]);
  }

  showIngredients() {
    let { ingredients, name } = this;
    let ingredientsLength = ingredients.length;
    if (ingredientsLength !== 0) {
      return ingredients;
    }
  }
}

class Order {
  constructor(newOrder) {
    this.newOrder = newOrder;
    OurOrders.push(this.newOrder);
    this.render(orderCover);
  }

  render(orderCover) {
    let node = document.createElement('div');
    node.innerHTML = `
        <p><span>${this.newOrder.name}</span> (${this.newOrder.ingredients.join(', ')}) 
        <br>будет готов через ${this.newOrder.cookingTime} минут</p>`;

    orderCover.appendChild(node);
  }
}

class Filter {

  constructor(Ingredients) {
    this.Ingredients = Ingredients;
    this.renderFilter(filterCover);
  }

  renderFilter(filterCover) {
    let form = document.createElement('form');
    let select = document.createElement('select');
    select.classList.add('ingredients_select');
    let text = document.createElement('p');
    text.textContent = 'Выбрать гамбургер : ';
    form.appendChild(text);

    for (let i = 0; i < this.Ingredients.length; i++) {
      let option = document.createElement('option');
      option.setAttribute('value', this.Ingredients[i]);
      option.textContent = this.Ingredients[i];
      select.appendChild(option);
    }
    let input1 = document.createElement('button');
    let input2 = document.createElement('button');
    input1.textContent = 'Найти';
    input2.textContent = 'Исключить';
    input1.classList.add('find');
    input2.classList.add('except');
    form.appendChild(select);
    form.appendChild(input1);
    form.appendChild(input2);
    filterCover.appendChild(form);

    this.includeBurger = this.includeBurger.bind(this);
    this.exceptBurger = this.exceptBurger.bind(this);
    form.querySelector('.find').addEventListener('click', this.includeBurger);
    form.querySelector('.except').addEventListener('click', this.exceptBurger);
  }

  includeBurger(e) {
    e.preventDefault();
    let selector = document.querySelector('.ingredients_select');
    let selectValue = selector[selector.selectedIndex].value;

    this.findBurgers(true, selectValue);
  }

  exceptBurger(e) {
    e.preventDefault();
    let selector = document.querySelector('.ingredients_select');
    let selectValue = selector[selector.selectedIndex].value;

    this.findBurgers(false, selectValue);
  }

  findBurgers(rule, selectValue) {

    while (menuCover.firstChild) {
      menuCover.removeChild(menuCover.firstChild);
    }

    OurMenu.map(function (burger) {
      let find = burger.ingredients.some(function(elem) {
        return selectValue === elem;
      });

      if (find === rule) {
        let offer = new Burger(burger.name, burger.ingredients, burger.cookingTime, false);
      }
    })
  }
}

let menuCover = document.querySelector('.menu');
let filterCover = document.querySelector('.filter');
let orderCover = document.querySelector('.order');

let burger = new Burger('Гамбургер', ['Булка', 'Огурчик', 'Котлетка', 'Кетчуп'], 10);
let burger1 = new Burger('Чеддер', ['Булка', 'Огурчик', 'Котлетка', 'Кетчуп', 'Острый перец', 'Сыр Чеддер', 'Кунжут'], 15);
let burger2 = new Burger('Виолла', ['Булка', 'Маслины', 'Котлетка', 'Майонез', 'Сыр Виолла', 'Капуста'], 18);
let burger3 = new Burger('Гауда', ['Булка', 'Огурчик', 'Котлетка', 'Сыр Гауда', 'Кетчуп'], 15);
let burger4 = new Burger('Рыбный', ['Булка', 'Помидорка', 'Котлетка', 'Майонез', 'Рыбная котлета', 'Кунжут'], 10);
let burger5 = new Burger('Веган', ['Булка', 'Помидорка', 'Маслины', 'Кетчуп', 'Огурчик', 'Кунжут'], 10);
let burger6 = new Burger('Асорти', ['Булка', 'Огурчик', 'Котлетка', 'Бекон', 'Кетчуп', 'Сыр Гауда', 'Сыр Чеддер', 'Сыр Виолла', 'Помидорка'], 25);
let filter = new Filter(Ingredients, OurMenu);
